const express = require("express");
const router = express.Router();
const InfoRouter = require("./infoschema");
router.post("/",async(req,res)=>{
    var data = new InfoRouter({
        Name:req.body.Name,
        Age:req.body.age,
        City:req.body.City
    });
    await data.save();
    res.json(data);
    
})
router.get("/",async (req,res)=>{
    var findData = await InfoRouter.find();
    res.json(findData);
})

router.get("/",(req,res)=>{
    res.json("I am From router file");

})
module.exports= router;