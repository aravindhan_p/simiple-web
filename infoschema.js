const mongoose = require("mongoose");

const infoschema = mongoose.Schema({
    Name:{
        type:String,
        required:true,
        trim:true
    },
    Age:{
        type:Number,
        required:true
    },
    city:{
        type:String,
        required:true
    },
createdTime:{
    type:Date,
    default:Date.now
}
})
module.export = mongoose.model("Info",infoschema);